// tạo mảng số nguyên n
var numArr = [];
function themSoN() {
  var soNguyenN = document.getElementById("so-nguyen-n").value * 1;
  document.getElementById("so-nguyen-n").value = "";

  numArr.push(soNguyenN);
  document.getElementById("day_so_n").innerHTML = `${numArr}`;
}

// EX_1: tính tổng số dương
function tongSoDuong() {
  var tongSoDuong = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      tongSoDuong = tongSoDuong + currentNum;
    }
  }
  document.getElementById("ex_1").innerHTML = `${tongSoDuong}`;
}

// EX_2: đếm số dương
function demSoDuong() {
  var demSoDuong = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      demSoDuong++;
    }
  }
  document.getElementById("ex_2").innerHTML = `${demSoDuong}`;
}

// EX_3: tìm số nhỏ nhất
function soNhoNhat() {
  var soNhoNhat = numArr[0];
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum < soNhoNhat) {
      soNhoNhat = currentNum;
    }
  }
  document.getElementById("ex_3").innerHTML = `${soNhoNhat}`;
}

// EX_4: tìm số dương nhỏ nhất
function soDuongNhoNhat() {
  // tạo mảng mới dùng chứa các số dương
  var soDuongArr = [];

  // dùng vòng lặp lấy số dương từ mảng cũ và lưu vào trong mảng mới
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      soDuongArr.push(currentNum);

      // tìm số dương nhỏ nhất trong mảng mới
      var soDuongNhoNhat = soDuongArr[0];
      for (var index = 0; index < soDuongArr.length; index++) {
        var currentSoDuong = soDuongArr[index];
        if (currentSoDuong < soDuongNhoNhat) {
          soDuongNhoNhat = currentSoDuong;
        }
      }
    }
    // khi mảng mới không có phần tử thì kết quả trả về là không có số dương
    else {
      soDuongNhoNhat = `Không có số dương trong mảng`;
    }
  }
  document.getElementById("ex_4").innerHTML = `${soDuongNhoNhat}`;
}

// EX_5: tìm số chẵn cuối cùng
function soChanCuoiCung() {
  var soChanCuoiCung = -1;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum % 2 == 0) {
      soChanCuoiCung = currentNum;
    }
  }
  document.getElementById("ex_5").innerHTML = `${soChanCuoiCung}`;
}

// EX_6: đổi chỗ
function doiCho() {
  var index1 = document.getElementById("vi-tri-1").value * 1;
  var index2 = document.getElementById("vi-tri-2").value * 1;

  [numArr[index1], numArr[index2]] = [numArr[index2], numArr[index1]];
  document.getElementById("ex_6").innerHTML = `${numArr}`;
}

// EX_7: sắp xếp tăng dần
function sapXep() {
  var a;
  var b;
  numArr.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("ex_7").innerHTML = `${numArr}`;
}

// EX_8: tìm số nguyên tố đầu tiên
function soNguyenTo() {
  var soNguyenTo = -1;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 1 && currentNum % 2 != 0) {
      soNguyenTo = currentNum;
      break;
    }
  }
  document.getElementById("ex_8").innerHTML = `${soNguyenTo}`;
}

// EX_9: đếm số nguyên
// tạo mảng số thực
var sothucArr = [];
function themSoThuc() {
  var soThuc = document.getElementById("so-thuc").value * 1;
  document.getElementById("so-thuc").value = "";

  sothucArr.push(soThuc);
  document.getElementById("day_so_thuc").innerHTML = `${sothucArr}`;
}

// đếm số nguyên
function demSoNguyen() {
  var demSoNguyen = 0;
  for (var index = 0; index < sothucArr.length; index++) {
    var currentNum = sothucArr[index];
    if (Number.isInteger(currentNum)) {
      demSoNguyen++;
    }
  }
  document.getElementById("ex_9").innerHTML = `${demSoNguyen}`;
}

// EX_10: so sánh số lượng số âm và số dương
function soSanh() {
  var demSoDuong = 0;
  var demSoAm = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum < 0) {
      demSoAm++;
    }
    if (currentNum > 0) {
      demSoDuong++;
    }
  }
  var soSanh;
  if (demSoDuong > demSoAm) {
    soSanh = `Số lượng số dương > số âm`;
  } else if (demSoDuong < demSoAm) {
    soSanh = `Số lượng số dương < số âm`;
  } else {
    soSanh = `Số lượng số dương = số âm`;
  }
  document.getElementById("ex_10").innerHTML = `${soSanh}`;
}
